# backend for Hackersir 2018 ISWeek event

## Dependecies

- runtime
  - nodejs

- packages
  - express
    - quite useless atm
    - consider removing it
  - socket.io
    - handling websocket stuffs
  - mongoose
    - mongodb
  - redis
    - redis
    - may need more packages to intergrate
  - babel
    - I wanna write in ES6

## How to run

- spin up redis and mongo server
  - maybe docker-compose later

- install yarn

- `yarn build && yarn start`

## commands

- from client
  - user status maintain
    - login
      - send username
    - setName
      - send nickname
    - getName
    - getScore

  - game status maintain
    - start
      - start a game and search for match
    - cancel
      - stop waiting
    - getProblem
    - answer

- from server
  - result
    - most return comes in this
  - otheranswer
    - answer from other player
  - halt
    - stop game
  - offline
    - oppnent afk ( no keyboard though )
