export default {
  redisURI: process.env.REDISURI || 'redis://localhost',
  mongoURI: process.env.MONGOURI || 'mongodb://localhost',
};
