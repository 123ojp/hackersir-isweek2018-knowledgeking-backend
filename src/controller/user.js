import mongoose from 'mongoose';
import makeHelpers from './helpers';
import Player from '../lib/player';

const User = mongoose.model('User');

/* eslint-disable valid-typeof */
const validateData = function (data, fields) {
  const result = [];
  const dataFields = Object.keys(data);
  Object.entries(fields).forEach((v) => {
    const [f, t] = v;
    if (!dataFields.includes(f)) {
      result.push(`${f} is empty`);
    } else if (!(typeof data[f] === t)) {
      result.push(`${f} is wrong type `);
    }
  });
  return result;
};
/* eslint-enable valid-typeof */

const login = function (socket, data) {
  const [response, success, failure] = makeHelpers(this, socket, 'login');

  const validation = validateData(data, { username: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  User.collection.findAndModify(
    { username: data.username },
    [],
    { $setOnInsert: { username: data.username } },
    {
      new: true, // return new doc if one is upserted
      upsert: true, // insert the document if it does not exist
    },
  ).then((res) => {
    if (res.ok === 0) { throw (res.lastErrorObject); }

    return User.findById(res.value).then((user) => {
      const player = new Player(user, socket);

      player.save();

      if (user.nickname) {
        success({ mesg: 'ok', token: player.id });
      } else {
        response({ mesg: 'set your nickname plz', registered: false, token: player.id });
      }
    });
  }, (error) => {
    failure({ mesg: error });
  });
};

const setName = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'setName');

  const validation = validateData(data, { token: 'string', nickname: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    if (!player) {
      failure({ mesg: 'invalid token' });
      return;
    }
    const { user } = player;
    if (user) {
      if (!user.nickname) {
        user.nickname = data.nickname;
        user.save().then((res) => {
          success({ nickname: res.nickname });
        }).catch((error) => {
          failure({ mesg: error });
        });
      } else {
        failure({ mesg: 'already have nickname' });
      }
    } else {
      failure({ mesg: 'invalid user' });
    }
  });
};

const getName = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'getName');

  const validation = validateData(data, { token: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    if (!player) {
      failure({ mesg: 'invalid token' });
      return;
    }
    const { user } = player;
    if (user) {
      success({ nickname: user.nickname });
    } else {
      failure({ mesg: 'invalid user' });
    }
  });
};

const getScore = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'getScore');

  const validation = validateData(data, { token: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    if (!player) {
      failure({ mesg: 'invalid token' });
      return;
    }
    const { user } = player;
    if (user) {
      success({ score: user.score });
    } else {
      failure({ mesg: 'invalid user' });
    }
  });
};

const logout = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'logout');

  const validation = validateData(data, { token: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    if (!player) {
      failure({ mesg: 'invalid token' });
      return;
    }
    player.destroy().then(() => {
      success();
    }, (err) => {
      failure({ mesg: err });
    });
  });
};

export default {
  login,
  setName,
  getName,
  getScore,
  logout,
};
