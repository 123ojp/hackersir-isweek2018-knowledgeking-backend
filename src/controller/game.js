import redis from 'redis';
import Game from '../lib/game';
import makeHelpers from './helpers';

const mesg = '満開STEP BY STEPで進め! STEP OUT さぁ、飛び出せ! ワタシ色 開花最前線、上昇中〜♪';

function start(socket, data) {
  const [resp, succ, fail] = makeHelpers(this, socket, 'start');
  const game = new Game();
  succ({ mesg, room: socket.game.room_id });
}

function cancel(socket, data) {
  const [resp, succ, fail] = makeHelpers(this, socket, 'cancel');
}

function getProblem(socket, data) {
  const [resp, succ, fail] = makeHelpers(this, socket, 'getProblem');
}

function answer(socket, data) {
  const [resp, succ, fail] = makeHelpers(this, socket, 'answer');
}

export default {
  start,
  cancel,
  getProblem,
  answer,
};
