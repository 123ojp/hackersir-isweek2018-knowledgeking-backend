import crypto from 'crypto';
import { redisSetAsync, redisGetAsync, redisDelAsync } from './redis_helpers';
import User from '../db/User';

/*
 * Player Class, or should call it Session Class
 *
 * Initiate: At user login
 * Contains: User data/id, socket id
 */
class Player {
  constructor(user, socket, id) {
    if (!id) {
      this.id = `player#${crypto.randomBytes(16).toString('hex')}`;
    } else {
      this.id = id;
    }
    this.socket = socket;
    this.user = user;
    this.state = '';
  }

  toString() {
    const obj = {};
    obj.id = this.id;
    obj.user = this.user.id;
    obj.socket = this.socket.id;
    obj.state = this.state;
    return JSON.stringify(obj);
  }

  async save() {
    return redisSetAsync(this.socket.id, this.id)
      .then(() => redisSetAsync(this.user.id, this.id))
      .then(() => redisSetAsync(this.id, this.toString()));
  }

  async destroy() {
    return redisDelAsync(this.id)
      .then(() => redisDelAsync(this.user.id))
      .then(() => redisDelAsync(this.socket.id));
  }

  static async findSession(socket) {
    return redisGetAsync(socket.id);
  }

  static async checkUserOnline(uid) {
    return redisGetAsync(uid);
  }

  static async load(token, sock) {
    return redisGetAsync(token).then((data) => {
      if (data) {
        const obj = JSON.parse(data);
        const socket = sock.server.sockets.sockets[obj.socket];
        return User.findById(obj.user).then(user => new Player(user, socket, token));
      }
      return false;
    });
  }
}

export default Player;
