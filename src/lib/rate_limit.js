
class RateLimit {
  constructor(server, limit) {
    this.server = server;
    this.limit = limit;
  }

  checkLimit() {
    if (this.server.sockets.clients().filter(cli => !cli.disconnected).length >= this.limit) {
      return false;
    }
    return true;
  }
}

export default RateLimit;
