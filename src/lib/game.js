import crypto from 'crypto';
import Player from './player';
import { redisSetAsync, redisGetAsync } from './redis_helpers';

class Game {
  constructor() {
    const randid = crypto.randomBytes(8).toString('hex');
    this.room_id = `Room#${randid}`;
    this.state = '';
    this.players = [];
  }

  addPlayer(player) {
    let res = null;
    if (this.players.length >= 2) {
      res = false;
    } else {
      this.players.push(player);
      res = this;
    }
    return res;
  }

  toString() {
    const obj = {};
    obj.room_id = this.room_id;
    obj.state = this.state;
    obj.players = [];
    this.players.forEach((el) => {
      obj.players.push(el.id);
    });
    return JSON.stringify(obj);
  }

  async saveGame() {
    return redisSetAsync(this.room_id, this.toString());
  }

  static async loadGame(id) {
    return redisGetAsync(id).then((res) => {
      if (res) {
        const obj = JSON.parse(res);
        const game = new Game();
        game.room_id = obj.room_id;
        obj.players.forEach((el) => {
          const player = Player.load(el.id);
          game.players.push(player);
        });
      }
      return false;
    });
  }
}

export default Game;
