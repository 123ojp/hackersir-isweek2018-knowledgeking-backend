import mongoose from 'mongoose';

const QuizSchema = mongoose.Schema({
  question: { type: String, required: true },
  answer: { type: String, required: true },
  candidate: { type: [String], required: true },
});

export default mongoose.model('Quiz', QuizSchema);
