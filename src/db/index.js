import mongoose from 'mongoose';

import config from '../config';

import User from './User';
import Quiz from './Quiz';

mongoose.connect(`${config.mongoURI}/quiz`);

mongoose.connection.on('error', (err) => {
  console.error('encountered some problem');
  console.error(err);
});

mongoose.connection.once('open', () => {
  console.log('successfully connected to database server');
});

export default {
  User,
  Quiz,
};
