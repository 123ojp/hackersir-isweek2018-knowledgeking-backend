/* eslint-disable no-unused-vars */
import express from 'express';
import { createServer } from 'http';
import module from 'module';
import path from 'path';
import socketIO from 'socket.io';

import config from './config';
import {} from './db';
import Player from './lib/player';
import GameManager from './lib/game_manager';
import { user, game } from './controller';

const app = express();
const server = createServer(app);

const io = socketIO(server);

server.listen(3000, 'localhost');

app.use(express.static(path.join('./', 'public')));
app.get('/', (req, res) => res.send('Hello'));

io.on('connection', function (socket) {
  console.log(socket.server.sockets.sockets[socket.id].id);
  console.log(`client#${socket.id}`, 'connected');

  socket.on('flag', () => {
    socket.emit('frog', { mesg: 'FROG{H3R3_15_7H3_FR0G}' });
  });

  socket.on('login', user.login.bind(this, socket))
    .on('setName', user.setName.bind(this, socket))
    .on('getName', user.getName.bind(this, socket))
    .on('getScore', user.getScore.bind(this, socket))
    .on('logout', user.logout.bind(this, socket));

  socket.on('start', game.start.bind(this, socket))
    .on('cancel', game.cancel.bind(this, socket))
    .on('getProblem', game.getProblem.bind(this, socket))
    .on('answer', game.answer.bind(this, socket));

  socket.on('error', (error) => {
    console.error(`client#${socket.id}`, 'encountered error:', error);
  });

  socket.on('disconnect', (data) => {
    console.info(`client#${socket.id}`, 'left!!');
  });
});
